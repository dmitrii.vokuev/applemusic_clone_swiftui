//
//  CMTime+extension.swift
//  AppleMusic_swiftUI
//
//  Created by dmitrii on 16.09.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation
import AVKit

extension CMTime {
    
    func toDisplayString() -> String {
        guard !CMTimeGetSeconds(self).isNaN else { return "" }
        let totalSecond = Int(CMTimeGetSeconds(self))
        let seconds = totalSecond % 60
        let minutes = totalSecond / 60
        let timeFormarString = String(format: "%02d:%02d", minutes, seconds)
        return timeFormarString
    }
}
