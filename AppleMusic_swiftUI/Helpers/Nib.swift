//
//  Nib.swift
//  AppleMusic_swiftUI
//
//  Created by dmitrii on 17.09.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import UIKit

extension UIView {
    
    class func loadFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?[0] as! T
    }
}
