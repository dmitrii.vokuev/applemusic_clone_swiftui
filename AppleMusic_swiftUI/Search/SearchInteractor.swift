//
//  SearchInteractor.swift
//  AppleMusic_swiftUI
//
//  Created by dmitrii on 14.09.19.
//  Copyright (c) 2019 dmitrii. All rights reserved.
//

import UIKit

protocol SearchBusinessLogic {
  func makeRequest(request: Search.Model.Request.RequestType)
}

class SearchInteractor: SearchBusinessLogic {
    var networkServiese = NetworkService()
  var presenter: SearchPresentationLogic?
  var service: SearchService?
  
  func makeRequest(request: Search.Model.Request.RequestType) {
    if service == nil {
      service = SearchService()
    }
    
    switch request {
    case .getTracks(let searchTerm):
        presenter?.presentData(response: Search.Model.Response.ResponseType.presentFooterView)
        networkServiese.fetchTracks(searchText: searchTerm) { [weak self] (searchResponse) in
            self?.presenter?.presentData(response: Search.Model.Response.ResponseType.presentTracks(searchResponse: searchResponse))
        }
        print("Interactor getTracks")
    }
  }
  
}
