//
//  NetworkService.swift
//  AppleMusic_swiftUI
//
//  Created by dmitrii on 14.09.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class NetworkService {
    func fetchTracks(searchText: String, completion: @escaping (SearchResponse?) -> Void) {
        let url = "https://itunes.apple.com/search"
        let params = ["term":"\(searchText)",
            "limit":"10",
            "media":"music"]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseData { (dataResponse) in
            if let error = dataResponse.error {
                print("error recieving data", error)
                completion(nil)
                return
            }
            
            guard let data = dataResponse.data else { return }
            
            let jsonDecoder = JSONDecoder()
            do {
                let objects = try jsonDecoder.decode(SearchResponse.self, from: data)
                print("objects: ", objects)
                completion(objects)
                
            } catch let jsonError {
                print("failed to Decode Json", jsonError)
                completion(nil)
            }
            
            //            let someString = String(data: data, encoding: .utf8)
        }
        
        
    }
}

