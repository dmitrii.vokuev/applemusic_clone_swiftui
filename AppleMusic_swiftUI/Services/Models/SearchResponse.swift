//
//  SearchResponse.swift
//  AppleMusic_swiftUI
//
//  Created by dmitrii on 14.09.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation

struct SearchResponse: Decodable {
    let resultCount: Int
    var results: [Track]
}

struct Track: Decodable{
    var trackName: String
    var collectionName: String?
    var artistName: String
    var artworkUrl100: String?
    var previewUrl: String?
}
